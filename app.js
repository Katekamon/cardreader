const fs = require('fs') //เรียกใช้Libraryที่ใช้จัดการเกี่ยวกับไฟล์ชื่อ fs 
const { Reader } = require('thaismartcardreader.js') //เรียกใช้ Library smartcardreader.js
const path = require('path')

// const myReader = new Reader()

// //ติดต่อกับส่วน HW(เครื่องอ่าน smartCardReader)
// process.on('From Global Rejection -> Reason', (reason) => {
//     console.log('From Global Rejection -> Reason: ' + reason);
// });

// console.log('Waiting For Device !')
// myReader.on('device-activated', async (event) => { //device-activate คือ จะตรวจสอบว่า ต่อกับเครื่อง smartCardReder อยู่มั่ย ถ้าต่ออยู่มันจะ defalf ว่าconnectedเป็น true แต่ถ้าไม่ต่อจะเป็น false
//     console.log('Device-Activated')
//     console.log(event.name)
//     console.log('=============================================')
// })

// myReader.on('error', async (err) => { //func err ถ้าด้านบนตรวจสอบพบว่าไม่เชื่อต่อเครื่องมันจะขึ้น err  มันจะคืนค่าเพื่อบอกว่ามันไม่สามารถทำงานได้
//     console.log(err)
// })

// myReader.on('image-reading', (percent) => {
//     console.log(percent) //อ่านข้อมูลของรูปภาพ โดยจะทำการดึงเป็นเปอร์เซนต์จนครบ100เปอร์เซนต์ คือดึงได้ทั้งรูป
// })

// myReader.on('card-inserted', async (person) => { //func ที่เป็น async เพื่อที่จะกำหนดให้ดึงข้อมูลเสร็จเป็นที่ละอย่างๆละอย่างไป
//     const cid = await person.getCid() //ดึงรหัสบัตรปชช.13 หลัก
//     const first4Code = await person.getFirst4CodeUnderPicture() //ดึงรหัสใต้ภาพ 4 หลักแรก 
//     const last8Code = await person.getLast8CodeUnderPicture() //ดึงรหัสสใต้ภาพ 8 ตัวสุดท้าย
//     const thName = await person.getNameTH() //ดึงชื่อ-สกุลภาษาไทย
//     const enName = await person.getNameEN() //ดึงชื่อ-สกุลภาษาอังกฤษ
//     const dob = await person.getDoB() //ดึง Date of Birth
//     const issueDate = await person.getIssueDate() //ดึงวันออกบัตร
//     const expireDate = await person.getExpireDate() //ดึงวันหมดอายุของบัตร
//     const address = await person.getAddress() //ดึงที่อยู่
//     const issuer = await person.getIssuer() //ดึงสถานที่ออกบัตร

//     //รอดึงข้อมูลจนครบแล้วจึงมาแสดงผล
//     console.log(`CitizenID: ${cid}`) //แสดงรหัสบัตรปชช. 13 หลัก
//     console.log(`First4CodeUnderPicture: ${first4Code}`) //
//     console.log(`Last8CodeUnderPicture: ${last8Code}`)
//     console.log(`THName: ${thName.prefix} ${thName.firstname} ${thName.lastname}`)
//     console.log(`ENName: ${enName.prefix} ${enName.firstname} ${enName.lastname}`)
//     console.log(`DOB: ${dob.day}/${dob.month}/${dob.year}`)
//     console.log(`Address: ${address}`)
//     console.log(`IssueDate: ${issueDate.day}/${issueDate.month}/${issueDate.year}`)
//     console.log(`Issuer: ${issuer}`)
//     console.log(`ExpireDate: ${expireDate.day}/${expireDate.month}/${expireDate.year}`)

//     console.log('=============================================')
//     console.log('Receiving Image')
//     const photo = await person.getPhoto()
//     console.log(`Image Saved to ${path.resolve('')}/${cid}.bmp`) //เลือกว่าจะ save รุปภาพที่ไหน
//     console.log('=============================================')
//     const fileStream = fs.createWriteStream(`${cid}.bmp`) //จะได้รับข้อมูลรูปภาพมาเป็นรหัส
//     const photoBuff = Buffer.from(photo) //ทำการแปลงรหัสรูปภาพที่ได้มา จากรหัส -> buffer 
//     fileStream.write(photoBuff) //เขียนไฟล์ภาพที่ได้จาก buffer
//     fileStream.close() //ปิดไฟล์ที่เขียน
// })

// myReader.on('card-removed', () => { // card-removed คือถ้าไม่มีการเสียบบัตรปชช.อยู่จะแสดง card removed ให้ทราบ
//     console.log('card removed')
// })

// myReader.on('device-deactivated', () => { //device-deactivated คือถ้า device มีปัญหาจะแสดง device-deactivated
//     console.log('device-deactivated')
// })